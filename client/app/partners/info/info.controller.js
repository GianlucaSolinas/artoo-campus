angular.module('artoo').controller('PartnersInfoCtrl', function ($scope, $log, partner) {
    var partner3 = {
        name: 'HM',
        vat_code: 'pi567',
        cod_fisc: 'cf456',
        address: 'Via repubblica',
        zip_code: '40121',
        city: 'Bologna',
        prov: 'BO',
        country: 'Italy',
        email: 'nike@esempio.it',
        username: 'nike',
        pass: 'pass456',
        active: 1,
        ultimo_acc: '28/10/2015',
        data_reg: '27/10/2015',
    };
    
    $scope.partner = partner;
    $log.log(partner);
    
});